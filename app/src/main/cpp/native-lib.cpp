#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;


extern "C" {


    JNIEXPORT void JNICALL
    Java_com_soapcopy_mycameraapp_shooting_ApplyFilter_toGray(JNIEnv *env, jobject instance,
                                                                jlong matAddrInput,
                                                                jlong matAddrResult) {

        // TODO
        Mat &matInput = *(Mat *)matAddrInput;
        Mat &matResult = *(Mat *)matAddrResult;

        cvtColor(matInput, matResult, CV_RGBA2GRAY);

    }


    JNIEXPORT void JNICALL
    Java_com_soapcopy_mycameraapp_filtering_FilterPresenter_toGray(JNIEnv *env, jobject instance,
                                                                             jlong matAddrInput,
                                                                             jlong matAddrResult) {

        Mat &matInput = *(Mat *)matAddrInput;
        Mat &matResult = *(Mat *)matAddrResult;

        cvtColor(matInput, matResult, CV_RGBA2GRAY);

    }

}
