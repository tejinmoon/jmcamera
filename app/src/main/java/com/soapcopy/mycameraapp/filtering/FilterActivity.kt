package com.soapcopy.mycameraapp.filtering

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.soapcopy.mycameraapp.R
import kotlinx.android.synthetic.main.activity_filter.*

class FilterActivity : AppCompatActivity(), FilterContract.View {

    private lateinit var filterPresenter: FilterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)


        filterPresenter = FilterPresenter().apply {
            attachView(this@FilterActivity)
        }

        filterPresenter.loadImgToBitmap("temporary.png")

        setButtons()

    }

    override fun updatePic(filename: String) {

        Log.d("Filter", "loading $filename")

    }

    override fun updatePic(bitmap: Bitmap) {
        img_after.setImageBitmap(bitmap)
    }

    fun setButtons() {
        radio1.setOnClickListener {
            //되는 거
            filterPresenter.filterGray(img_after)
        }

        radio2.setOnClickListener {
            //안되는 거
            filterPresenter.filterGray2()
        }

        radio3.setOnClickListener {
            filterPresenter.filterEqualization()
        }

        radio4.setOnClickListener {

        }
    }


}
