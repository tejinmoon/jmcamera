package com.soapcopy.mycameraapp.filtering

import android.graphics.Bitmap
import android.widget.ImageView

/**
 * Created by Dev on 2017-06-13.
 */
interface FilterContract {

    interface View {

        fun updatePic(filename: String)

        fun updatePic(bitmap: Bitmap)
    }

    interface Presenter {

        var view: View
        fun attachView(view: View)

        fun loadImgToBitmap(filename: String)

        fun filterGray(view: ImageView)

        fun filterGray2()

        fun filterEdge()

        fun filterGaussian()

        fun filterEqualization()
    }

}