package com.soapcopy.mycameraapp.filtering;

import android.graphics.Bitmap
import android.util.Log
import android.widget.ImageView
import com.soapcopy.mycameraapp.shooting.ApplyFilter
import com.soapcopy.mycameraapp.util.ImageLoader
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.opencv.android.Utils
import org.opencv.core.Mat

/**
 * Created by Dev on 2017-06-13.
 */

class FilterPresenter : FilterContract.Presenter {

    lateinit override var view: FilterContract.View
    lateinit var original: Bitmap
    lateinit var imgInput: Mat

    external fun toGray(matAddrInput: Long?, matAddrResult: Long?)

    override fun attachView(view: FilterContract.View) {
        this.view = view
    }

    override fun loadImgToBitmap(filename: String) {

        Log.d("Filter", "load image to bitmap 6")

        Observable.just(filename)
                .observeOn(Schedulers.computation())
                .map { s -> ImageLoader.load(s) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { b -> run {
                    original = b

                    imgInput = Mat()
                    Utils.bitmapToMat(original, imgInput)

                    view.updatePic(original)

                    Log.d("Filter", "W : "+original.width + " H : "+original.height)
                }}
    }

    // 되는 거
    override fun filterGray(view : ImageView) {

        ApplyFilter.toGray(imgInput, view)

    }

    // 안되는 거
    override fun filterGray2() {
        val imgOutput = Mat()
        val filtered: Bitmap = Bitmap.createBitmap(original.width, original.height, original.config)

//        Utils.bitmapToMat(original, imgInput)

        toGray(imgInput.nativeObjAddr, imgOutput.nativeObjAddr)

        Utils.matToBitmap(imgOutput, filtered)

        view.updatePic(filtered)

        Log.d(TAG,"filterGray2 succeed")
    }

    override fun filterEqualization() {
        view.updatePic(original)
    }

    override fun filterGaussian() {

    }

    override fun filterEdge() {

    }

    companion object {

        private val TAG = "FilterPresenter"

        init {
            System.loadLibrary("opencv_java3")
            System.loadLibrary("native-lib")
        }

    }

}

//class FilterPresenter implements FilterContract.Presenter {
//
//        FilterContract.View view;
//        Bitmap original;
//        Bitmap filtered;
//
//        Mat imgInput;
//        Mat imgOutput;
//
//
//
//
//    public native void t2();
////    public native void jniTest(int a, int b);
//    public native int jniTest(int a, int b);
//    public native void ConvertRGBtoGray(long matAddrInput, long matAddrResult);
//
//    @Override
//    public void attachView(@NotNull FilterContract.View view) {
//        this.view = view;
//    }
//
//    @Override
//        public void loadImgToBitmap(@NotNull String filename) {
////                Observable.just(filename)
////                        .observeOn(Schedulers.computation())
////                        .map ( s -> ImageLoader.INSTANCE.load$production_sources_for_module_app(s) )
////                        .observeOn(AndroidSchedulers.mainThread())
////                        .subscribe ( b ->  {
////                                original = (Bitmap)b;
////                                view.updatePic(original);
////
////                                Log.d("Filter", "W : "+original.getWidth() + " H : "+original.getHeight());
////                        });
//        }
//
//        @Override
//        public void filterGray() {
//            Log.d("Filter","convert to grayscale$$");
//
//
//        }
//
//        @Override
//        public void filterGaussian() {
//
//        }
//
//        @Override
//        public void filterEqualization() {
//
//        }
//
//        @Override
//        public void filterEdge() {
//
//        }
//
////    @NotNull
////    @Override
////    public FilterContract.View getView() {
////        return view;
////    }
////
////    @Override
////    public void setView(@NotNull FilterContract.View view) {
////        this.view = view;
////
////    }
//
//    //        override fun filterGray() {
////
////        Log.d("Filter","convert to grayscale@")
////
////        Utils.bitmapToMat(original, imgInput)
////        Log.d("Filter","Channels : "+imgInput?.channels() + "/ Type : " + imgInput?.type())
////
////        imgOutput = Mat(imgInput!!.rows(), imgInput!!.cols(), imgInput!!.type())
////
////        var a: Int = 1
////        var b: Int = 2
////
////        jniTest(a,b)
////
////        Log.d("Filter", "result : " + b);
////
//////        ConvertRGBtoGray(imgInput?.nativeObjAddr, imgOutput?.nativeObjAddr)
//////        Utils.matToBitmap(imgOutput, filtered)
//////
//////        view.updatePic(filtered)
////        }
//
//
//}

