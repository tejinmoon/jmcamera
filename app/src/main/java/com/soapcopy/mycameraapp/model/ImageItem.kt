package com.soapcopy.mycameraapp.model

/**
 * Created by Dev on 2017-06-13.
 */
class ImageItem {

    var width: Int = 0
    var height: Int = 0
}