package com.soapcopy.mycameraapp.shooting

import android.graphics.Bitmap
import android.util.Log
import android.widget.ImageView
import org.opencv.android.Utils
import org.opencv.core.Mat

/**
 * Created by Dev on 2017-06-15.
 */
object ApplyFilter {
    external fun toGray(matAddrInput: Long, matAddrResult: Long)

    internal fun toGray(imgInput: Mat, view: ImageView) {
        Log.d("ApplyFilter","toGray")

        val imgOutput = Mat()
        val filtered = Bitmap.createBitmap(imgInput.cols(), imgInput.rows(), Bitmap.Config.ARGB_8888)

        toGray(imgInput.nativeObjAddr, imgOutput.nativeObjAddr)
        Utils.matToBitmap(imgOutput, filtered)

        view.setImageBitmap(filtered)

    }
}