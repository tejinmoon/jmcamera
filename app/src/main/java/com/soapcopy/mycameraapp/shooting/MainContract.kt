package com.soapcopy.mycameraapp.shooting

import android.graphics.Bitmap
import com.soapcopy.mycameraapp.filtering.FilterContract
import com.soapcopy.mycameraapp.util.MyCameraView

/**
 * Created by Dev on 2017-06-12.
 */
interface MainContract {

    interface View {

        fun toFilter()

        fun showToast(message: String)
    }

    interface Presenter {

        var view: MainContract.View
        fun attachView(view: MainContract.View)

        fun takePhoto(surfaceView: MyCameraView)

        fun handlePhoto(bitmap: Bitmap)

    }
}