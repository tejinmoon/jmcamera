package com.soapcopy.mycameraapp.shooting

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.os.Environment
import android.util.Log
import com.soapcopy.mycameraapp.util.MyCameraView
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

/**
 * Created by Dev on 2017-06-13.
 */
class MainPresenter : MainContract.Presenter {

    lateinit override var view: MainContract.View
    lateinit var bitmap: Bitmap

    private var capturePhotoThread: Thread? = null

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun takePhoto(surfaceView: MyCameraView) {
        if (capturePhotoThread==null || !capturePhotoThread!!.isAlive) {

            surfaceView.takePhoto(Camera.PictureCallback { data, camera ->
                Log.d(TAG, "photo taked")

                capturePhotoThread = object : Thread() {
                    override fun run() {
                        try {
                            bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                            handlePhoto(bitmap)
                        } catch (e: OutOfMemoryError) {

                        }

                    }

                    override fun destroy() {
                        super.destroy()
                    }
                }

                capturePhotoThread?.start()

                surfaceView.mCamera?.stopPreview()
            })
        }
    }

    override fun handlePhoto(bitmap: Bitmap) {
        val EXTERNAL_PATH = Environment.getExternalStorageDirectory().absolutePath

        val saveDirFile = File(EXTERNAL_PATH + "/myCam")
        if (!saveDirFile.exists()) {
            saveDirFile.mkdirs()
        }

        val nomediaFile = File(EXTERNAL_PATH + "/myCam/.nomedia")
        if (!nomediaFile.exists()) {
            Log.d(TAG, "nomedia not exists")
            try {
                nomediaFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else {
            Log.d(TAG, "nomedia exists")
        }

        val file = File(EXTERNAL_PATH + "/myCam/temporary.png")
        val filestream: FileOutputStream
        try {
            Log.d(TAG, "file : " + file.absolutePath)

            filestream = FileOutputStream(file)
            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, filestream)


            filestream.flush()
            filestream.close()

            //성공 시 필터 액티비티로
            view.toFilter()

            Log.d(TAG, "photo saved")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()

            //실패 시 토스트 출력
            view.showToast("사진 촬영에 실패하였습니다.")
            Log.d(TAG, "photo fail")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    companion object {
        val TAG = "MainPresenter"
    }
}