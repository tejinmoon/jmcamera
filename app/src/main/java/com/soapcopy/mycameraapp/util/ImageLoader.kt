package com.soapcopy.mycameraapp.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import com.soapcopy.mycameraapp.model.ImageItem
import java.io.File

/**
 * Created by Dev on 2017-06-13.
 */

object ImageLoader {

    internal fun load(filename: String): Bitmap {

        Log.d("ImageLoader", "On " + Thread.currentThread().name)
        Log.d("ImageLoader", "load image $filename to bitmap~")

        val EXTERNAL_PATH: String = Environment.getExternalStorageDirectory().absolutePath
        var outputFile: File? = File("$EXTERNAL_PATH/myCam/$filename")
        var imageBefore: ImageItem = ImageItem()

        val options: BitmapFactory.Options = BitmapFactory.Options().apply {
            inJustDecodeBounds = true
            inPreferredConfig = Bitmap.Config.ARGB_8888
        }

        // 어떤 의미를 갖는가?
//        BitmapFactory.decodeFile(outputFile?.absolutePath, null)

        imageBefore.width = options.outWidth
        imageBefore.height = options.outHeight

        options.inJustDecodeBounds = false
        options.inSampleSize = Math.max(imageBefore.width/2000, imageBefore.height/2000)

        return BitmapFactory.decodeFile(outputFile?.absolutePath, null)
    }
}
