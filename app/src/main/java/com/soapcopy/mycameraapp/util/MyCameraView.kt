package com.soapcopy.mycameraapp.util

import android.app.Activity
import android.content.Context
import android.hardware.Camera
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import org.opencv.android.JavaCameraView
import java.io.FileOutputStream
import java.io.IOException

/**
 * Created by Dev on 2017-06-12.
 */
class MyCameraView : SurfaceView, SurfaceHolder.Callback {

    private val TAG: String = "myCameraView"

    lateinit private var mContext: Context
    lateinit public var mHolder: SurfaceHolder
    public var mCamera: Camera? = null
    public var previewSizes: List<Camera.Size>? = null

    constructor(context: Context) : super(context) {
        mContext = context
        mHolder = holder
        mHolder.addCallback(this)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        mHolder = holder
        mHolder.addCallback(this)
    }


    fun takePhoto(handler: Camera.PictureCallback) : Boolean {
        mCamera?.takePicture(null, null, handler)?.let{
            return true
        }
        return false
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        try {
            mCamera = Camera.open()
            previewSizes = mCamera?.parameters?.supportedPreviewSizes
        } catch (e: Exception){
            mCamera = null
        }

        if(mCamera == null) {
            (mContext as Activity).finish()
            return
        }

        try {
            mCamera?.setPreviewDisplay(holder)

            val m_resWidth: Int = 1920
            val m_resHeight: Int = 1080

            val parameters: Camera.Parameters? = mCamera?.parameters

            parameters?.setPictureSize(m_resWidth, m_resHeight)

            mCamera?.parameters = parameters

        } catch (e: IOException){
            mCamera?.release()
            mCamera = null
        }

        if(mCamera == null)
            (mContext as Activity).finish()

    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        if(mCamera == null) return

        if(holder?.surface == null) return

        try {
            mCamera?.stopPreview()
        } catch (e: Exception){

        }

        val parameters: Camera.Parameters? = mCamera?.parameters

        val focusModes: List<String>? = parameters?.supportedFocusModes
        if(focusModes?.contains(Camera.Parameters.FOCUS_MODE_AUTO)?:false){
            Log.d(TAG, "on focus mode auto")
            parameters?.focusMode = Camera.Parameters.FOCUS_MODE_AUTO
        } else {
            parameters?.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
        }

        parameters?.set("orientation", "portrait")
        mCamera?.setDisplayOrientation(90)
        parameters?.setRotation(90)
        mCamera?.parameters = parameters

        mCamera?.startPreview()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        if(mCamera == null) return

        mCamera?.stopPreview()
        mCamera?.release()
        mCamera = null
    }
}